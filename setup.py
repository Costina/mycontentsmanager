from setuptools import setup, find_packages

def main():

    setup(
        name='mycontentsmanager',
        packages=find_packages(),
        include_package_data=True,
        zip_safe=False,
    )


if __name__ == '__main__':
    main()