try:
    import IPython
except ImportError:
    raise ImportError(
        "No IPython installation found."
    )

from .mymanager import MyContentsManager

__all__ = [
    'MyContentsManager',
]